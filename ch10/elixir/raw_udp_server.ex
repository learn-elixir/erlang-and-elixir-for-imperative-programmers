defmodule RawUdpServer do
  use GenServer
  require Logger

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    {:ok, _socket} = :gen_udp.open(4242)
  end

  def handle_info({:udp, _socket, _ip, _port, data}, state) do
    Logger.info "Received a message: " <> inspect(data)
    {:noreply, state}
  end
end

### sample usage
###
### server
###
# iex(1)> c("raw_udp_server.ex")
# [RawUdpServer]
# iex(2)> RawUdpServer.start_link
# {:ok, #PID<0.88.0>}
# iex(3)>
# 08:47:41.818 [info]  Received a message: 'hi server'
###
### client (Erlang)
###
# 1> rawudp:client("hi server",4242).
# client opened socket:#Port<0.447>
# 0
