%%%-------------------------------------------------------------------
%%% @author luhtonen
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. Mar 2017 08:03
%%%-------------------------------------------------------------------
-module(rawudp).
-author("luhtonen").

%% API
-export([start/1, client/2]).

start(Port) ->
  {ok, Socket} = gen_udp:open(Port, [binary, {active, false}]),
  io:format("server opened socket:~p~n", [Socket]),
  loop(Socket).

loop(Socket) ->
  inet:setopts(Socket, [{active, once}]),
  receive
    {udp, Socket, Host, Port, Bin} ->
      io:format("server received:~p~n", [Bin]),
      gen_udp:send(Socket, Host, Port, <<"From Server:", Bin/binary>>),
      case Bin of
        <<"stop">> -> gen_udp:close(Socket);
        _ -> loop(Socket)
      end
  end.

client(N, Port) ->
  {ok, Socket} = gen_udp:open(0, [binary]),
  io:format("client opened socket:~p~n", [Socket]),
  ok = gen_udp:send(Socket, "localhost", Port, N),
  Value = receive
            {udp, Socket, _, _, Bin} ->
              io:format("client received:~p~n", [Bin])
          after 1000 ->
              0
          end,
  gen_udp:close(Socket),
  Value.

%%% sample usage
%%%
%%% server
%%%
% 1> c(rawudp).
% {ok,rawudp}
% 2> rawudp:start(4242).
% server opened socket:#Port<0.2077>
% server received:<<"hi server">>
% server received:<<"stop">>
% ok
%%%
%%% client
%%%
% 1> rawudp:client("hi server",4242).
% client opened socket:#Port<0.439>
% client received:<<"From Server:hi server">>
% ok
% 2> rawudp:client("stop",4242).
% client opened socket:#Port<0.446>
% client received:<<"From Server:stop">>
% ok
