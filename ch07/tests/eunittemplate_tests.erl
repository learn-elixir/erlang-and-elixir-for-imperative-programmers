%%% @author Eduard Luhtonen <luhtonen@gmail.com>
%%% @copyright 2017 Eduard Luhtonen
%%% @doc
%%%
%%% @end

-module(eunittemplate_tests).
-author('Eduard Luhtonen luhtonen@gmail.com').

-define(NOTEST, true).
-define(NOASSERT, true).
-include_lib("eunit/include/eunit.hrl").

-define(MODNAME, eunittemplate).

%%% test generator
eunittemplate_test_() ->
  [].
