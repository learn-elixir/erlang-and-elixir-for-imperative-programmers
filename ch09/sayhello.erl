%%% -------------------------------------------------------------------
%%% @doc
%%% This module is a basic example of
%%% (1) sending messages between processes and
%%% (2) distribute processes on different nodes / machines.
%%% Usage:
%%% Machine 1: erl -sname precipient -setcookie scookie
%%% sayhello:start_recipient().
%%% Machine 2: erl -sname psayhello -setcookie scookie
%%% sayhello:start_sayhello('precipient@machinename').
%%% @end
%%%-------------------------------------------------------------------
-module(sayhello).

%% API
-export([start/0, recipient/0, say_hello/2, say_hello/3, start_recipient/0, start_sayhello/1]).

%% methods for distributed processes on more than one machine
say_hello(_, 0, Node) ->
  io:format("Process arity 3 'say hello' finished~n", []),
  {precipient, Node} ! finished;

say_hello(What, Times, Node) ->
  io:format("3: ~p~n", [What]),
  {precipient, Node} ! What,
  say_hello(What, Times - 1, Node).

start_recipient() ->
  register(precipient, spawn(sayhello, recipient, [])).

start_sayhello(InitNode) ->
  spawn(sayhello, say_hello, ["Hello", 2, InitNode]).

%% methods for processes on one machine
say_hello(_, 0) ->
  io:format("Process arity 2 'say_hallo' finished~n", []),
  precipient ! finished;
say_hello(What, Times) ->
  io:format("2: ~p~n", [What]),
  precipient ! What,
  say_hello(What, Times - 1).

%% recipient of say_hello-messages
recipient() ->
  receive
    finished ->
      io:format("Recipient process finished~n", []);
    log ->
      io:format("Recipient received log message~n", []),
      recipient();
    What ->
      io:format("Recipient received ~s~n", [What]),
      recipient()
  end.

start() ->
  register(precipient, spawn(sayhello, recipient, [])),
  precipient ! nomessagedefined,
  spawn(sayhello, say_hello, ["Hello", 4]).

%% same machine / shell run example
%% 1> c("sayhello").
%% {ok,sayhello}
%% 2> sayhello:start().
%% Recipient received nomessagedefined
%% 2: "Hello"
%% <0.66.0>
%% 2: "Hello"
%% Recipient received Hello
%% 2: "Hello"
%% Recipient received Hello
%% 2: "Hello"
%% Recipient received Hello
%% Process arity 2 'say_hallo' finished
%% Recipient received Hello
%% Recipient process finished

%% multiple machines / shells run example
%%
%% machine 1
%%
%% $ erl -sname precipient -setcookie scookie
%% Erlang/OTP 19 [erts-8.2.2] [source] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false] [dtrace]
%%
%% Eshell V8.2.2  (abort with ^G)
%% (precipient@Eduards-MacBook-Pro)1> sayhello:start_recipient().
%% true
%% Recipient received Hello
%% Recipient received Hello
%% Recipient process finished
%% (precipient@Eduards-MacBook-Pro)2>
%%
%% machine 2
%%
%% $ erl -sname psayhello -setcookie scookie
%% Erlang/OTP 19 [erts-8.2.2] [source] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false] [dtrace]
%%
%% Eshell V8.2.2  (abort with ^G)
%% (psayhello@Eduards-MacBook-Pro)1> sayhello:start_sayhello('precipient@Eduards-MacBook-Pro').
%% 3: "Hello"
%% <0.65.0>
%% 3: "Hello"
%% Process arity 3 'say hello' finished
%% (psayhello@Eduards-MacBook-Pro)2>
