defmodule SayHello do
@doc """
This module is a basic example of
  (1) sending messages between processes and
  (2) distribute processes on different machines.
Usage:
Machine 1: iex --sname precipient --cookie scookie
           SayHello.start_recipient
Machine 2: iex --sname pinit --cookie scookie
           Node.connect :"precipient@machinename"
           SayHello.start_sayhello
"""
def start_recipient do
  precipient = spawn(SayHello, :recipient, [])
  :global.register_name(:precipient, precipient)
end

def start_sayhello() do
  spawn(SayHello, :say_hello, ["Hello", 2])
  send :global.whereis_name(:precipient), :log
end

def say_hello(_, 0) do
  IO.puts "Process 'say_hello' finished"
  send :global.whereis_name(:precipient), :finished
end

def say_hello(what, times) do
  IO.puts what
  send :global.whereis_name(:precipient), what
  say_hello(what, times - 1)
end

# recipient of say_hello-messages
def recipient do
  receive do
    :finished ->
      IO.puts "Recipient process finished"
    :log ->
      IO.puts "Recipient received log message"
      recipient
    what ->
      IO.puts "Recipient received #{what}"
      recipient
  end
end

def start do
  precipient = spawn(SayHello, :recipient, [])
  :global.register_name(:precipient, precipient)
  send precipient, :nomessagedefined
  spawn(SayHello, :say_hello, ["Hello", 4])
end

end

## same machine / shell run example
# iex(1)> c("sayhello.ex")
# [SayHello]
# iex(2)> SayHello.start
# Hello
# Recipient received nomessagedefined
# Hello
# Recipient received Hello
# Hello
# Recipient received Hello
# Hello
# Recipient received Hello
# Process 'say_hello' finished
# Recipient received Hello
# Recipient process finished
# #PID<0.92.0>

## multiple machines / shells run example
##
## machine 1
##
# $ iex --sname precipient --cookie scookie
# Erlang/OTP 19 [erts-8.2.2] [source] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false] [dtrace]
#
# Interactive Elixir (1.4.2) - press Ctrl+C to exit (type h() ENTER for help)
# iex(precipient@Eduards-MacBook-Pro)1> c("sayhello.ex")
# [SayHello]
# iex(precipient@Eduards-MacBook-Pro)2> SayHello.start_recipient
# :yes
# Recipient received log message
# Recipient received Hello
# Recipient received Hello
# Recipient process finished
# iex(precipient@Eduards-MacBook-Pro)3>
##
## machine 2
##
# $ iex --sname pinit --cookie scookie
# Erlang/OTP 19 [erts-8.2.2] [source] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false] [dtrace]
#
# Interactive Elixir (1.4.2) - press Ctrl+C to exit (type h() ENTER for help)
# iex(pinit@Eduards-MacBook-Pro)1> c("sayhello.ex")
# [SayHello]
# iex(pinit@Eduards-MacBook-Pro)2> Node.connect :"precipient@Eduards-MacBook-Pro"
# true
# iex(pinit@Eduards-MacBook-Pro)3> SayHello.start_sayhello
# Hello
# Hello
# Process 'say_hello' finished
# :log
# iex(pinit@Eduards-MacBook-Pro)4>
